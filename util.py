import os
import re
import numpy as np

import matplotlib as mlp
mlp.use("Agg")
import matplotlib.pyplot as plot

def notify(email: str, task_output_path: str, zip_filename: str) -> None:
    """ Notify to the email that the process is ended. """
    import smtplib
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email.mime.multipart import MIMEMultipart
    from email import encoders

    msg = MIMEMultipart()
    msg["Subject"] = "Hey, look at Unipg Machine Learning Beast!"
    msg["From"] = "ml.unipg.dev@unipg.it"
    msg["To"] = email

    with open("%s/%s" % (task_output_path, zip_filename), encoding="ISO-8859-1") as f:
        mbase = MIMEBase("application", "zip")
        mbase.set_payload(f.read())
        encoders.encode_base64(mbase)
        mbase.add_header('Content-Disposition', 'attachment', filename=os.path.basename(zip_filename))
        msg.attach(mbase)

    s = smtplib.SMTP("smtp.gmail.com:587")
    s.ehlo()
    s.starttls()
    s.login("belli.valerio@gmail.com", "daqpxbhhxotkjzdy")
    s.send_message(msg)
    s.quit()


def create_flow_plot(task_output_path: str, position: str) -> None:
    """ Create a plot starting from the execution data of a test """
    fig = plot.figure()
    plot_data = []
    line_regex = re.compile("\|-.(\d+).+ACC_VAL:([0-9.]+).+TIME:([0-9.]+)")
    last_idx = -1
    with open("%s/%s" % (task_output_path, position)) as f:
        for plot_step in f.readlines():
            res = line_regex.match(plot_step.strip())
            if res is not None:
                if last_idx < int(res[1]) or last_idx is None:
                    last_idx = int(res[1])
                    plot_data.append([float(res[2]), float(res[3])])
    plot_data = np.stack(plot_data)
    ax = fig.add_subplot(111)

    ax.plot(plot_data[:, 1], plot_data[:, 0], "k", linewidth=0.6)
    ax.set_xlabel("Cost")
    ax.set_ylabel("Time (msec)")
    fig.savefig("%s/%s" % (task_output_path, os.path.splitext(os.path.basename(position))[0]))